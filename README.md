# Preseed

[![Pipeline Status](https://gitlab.com/dedmin/preseed/badges/master/pipeline.svg)](https://gitlab.com/dedmin/preseed/commits/master) 

# Introduction

Preseeding provides a way to set answers to questions asked during the installation process, without having to manually enter the answers while the installation is running. This makes it possible to fully automate most types of installation and even offers some features not available during normal installations. 

# Configuration

run nginx with preseed config:
```bash
docker pull dedmin/preseed
wget "https://gitlab.com/dedmin/preseed/-/raw/master/run.sh"
./run.sh
```
or

```bash
git clone https://gitlab.com/dedmin/preseed
cd preseed
docker build . -t dedmin/preseed
./run.sh
```
